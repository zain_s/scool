#include "scool/simple_executor.hpp"

#include "toy_task.hpp"
#include "toy_state.hpp"

#include <iostream>


int main(int argc, char* argv[]) {
    toy_task::n = 3;
    scool::simple_executor<toy_task, toy_state> exec;

    exec.init(toy_task(true), toy_state());

    while (exec.step() > 0) {
        std::cout << "step: " << exec.iteration()
                  << ", processed tasks: " << exec.state().count
                  << std::endl;
    }

    return 0;
} // main
