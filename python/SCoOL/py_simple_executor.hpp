#ifndef PY_SIMPLE_EXECUTOR_HPP
#define PY_SIMPLE_EXECUTOR_HPP

#include "simple_executor.hpp"

#include "universal_task.hpp"
#include "universal_state.hpp"

using simple_executor = scool::simple_executor<Py_Universal_Task, Universal_State>;
using simple_context = scool::simple_context<simple_executor, true>;

class Py_Simple_Executor : public simple_executor {
public:
    /* Inherit the constructors */
    using simple_executor::simple_executor;

    Py_Simple_Executor() {}

    simple_context& get_context() { return ctx_; }

}; // class Py_Simple_Executor

#endif // PY_SIMPLE_EXECUTOR_HPP
