from SCoOL import *

class TOY_State(State):
    pass

class TOY_Properties(Properties):
    n = 4
    level = -1
    p = [i for i in range(n)]

class TOY_Task(Task):
    def process(self, context: Simple_Context, state: TOY_State) -> None:
        props: TOY_Properties = self.properties
        props.level += 1

        if (props.level < props.n):
            print("p: {}, level: {}".format(props.p, props.level))

            for i in range(props.level, props.n):
                props.p[i], props.p[props.level] = props.p[props.level], props.p[i]
                new_task = TOY_Task(props)
                context.push(new_task)
                props.p[i], props.p[props.level] = props.p[props.level], props.p[i]
        else:
            print("complete:", props.p)

    def merge(self, task: object) -> None:
        pass


def main():
    properties: Properties = TOY_Properties()

    task: Task = TOY_Task(properties)
    state: State = TOY_State()

    executor: Simple_Executor = Simple_Executor()
    executor.init(task, state)

    while(executor.step() > 0):
        pass

if __name__ == "__main__":
    main()
